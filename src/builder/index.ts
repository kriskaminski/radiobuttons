/** Package information retrieved from `package.json` using webpack. */
declare const PACKAGE_NAME: string;
declare const PACKAGE_VERSION: string;

/** Dependencies */
import {
    Collection,
    NodeBlock,
    Slots,
    affects,
    conditions,
    definition,
    editor,
    npgettext,
    pgettext,
    slots,
    tripetto,
} from "tripetto";
import { Radiobutton } from "./radiobutton";
import { RadiobuttonCondition } from "./condition";

/** Assets */
import ICON from "../../assets/icon.svg";

@tripetto({
    type: "node",
    identifier: PACKAGE_NAME,
    version: PACKAGE_VERSION,
    icon: ICON,
    get label() {
        return pgettext("block:radiobuttons", "Radio buttons");
    },
})
export class Radiobuttons extends NodeBlock {
    @definition
    @affects("#label")
    buttons = Collection.of(Radiobutton, this as Radiobuttons);
    radioSlot!: Slots.String;

    get label() {
        return npgettext(
            "block:radiobuttons",
            "%2 (%1 option)",
            "%2 (%1 options)",
            this.buttons.count,
            this.type.label
        );
    }

    @slots
    defineSlots(): void {
        this.radioSlot = this.slots.static({
            type: Slots.String,
            reference: "button",
            label: pgettext("block:radiobuttons", "Selected button"),
        });
    }

    @editor
    defineEditor(): void {
        this.editor.groups.general();
        this.editor.name();
        this.editor.description();
        this.editor.explanation();

        this.editor.collection({
            collection: this.buttons,
            title: pgettext("block:radiobuttons", "Radio buttons"),
            placeholder: pgettext("block:radiobuttons", "Unnamed option"),
            editable: true,
            sorting: "manual",
        });

        this.editor.groups.options();
        this.editor.required(this.radioSlot);
        this.editor.visibility();
        this.editor.alias(this.radioSlot);
        this.editor.exportable(this.radioSlot);
    }

    @conditions
    defineConditions(): void {
        this.buttons.each((button: Radiobutton) => {
            if (button.name) {
                this.conditions.template({
                    condition: RadiobuttonCondition,
                    label: button.name,
                    props: {
                        slot: this.radioSlot,
                        button: button,
                    },
                });
            }
        });
    }
}
