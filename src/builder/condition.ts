/** Package information defined using webpack */
declare const PACKAGE_NAME: string;
declare const PACKAGE_VERSION: string;

/** Dependencies */
import {
    ConditionBlock,
    Forms,
    Layouts,
    affects,
    arraySize,
    definition,
    editor,
    isFilledString,
    pgettext,
    tripetto,
} from "tripetto";
import { Radiobuttons } from "./";
import { Radiobutton } from "./radiobutton";

/** Assets */
import ICON from "../../assets/icon.svg";

@tripetto({
    type: "condition",
    identifier: PACKAGE_NAME,
    version: PACKAGE_VERSION,
    context: Radiobuttons,
    icon: ICON,
    get label() {
        return pgettext("block:radiobuttons", "Radio button choice");
    },
})
export class RadiobuttonCondition extends ConditionBlock {
    @affects("#condition")
    @definition("buttons")
    button: Radiobutton | undefined;

    get name() {
        return (this.button ? this.button.name : "") || this.type.label;
    }

    get radiobuttons() {
        return (
            (this.node &&
                this.node.block instanceof Radiobuttons &&
                this.node.block) ||
            undefined
        );
    }

    @editor
    defineEditor(): void {
        if (this.node && this.radiobuttons) {
            const buttons: Forms.IDropdownOption<Radiobutton>[] = [];

            this.radiobuttons.buttons.each((button: Radiobutton) => {
                if (isFilledString(button.name)) {
                    buttons.push({
                        label: button.name,
                        value: button,
                    });
                }
            });

            if (arraySize(buttons) > 0) {
                this.editor.form({
                    title: this.node.label,
                    controls: [
                        new Forms.Dropdown<Radiobutton>(
                            buttons,
                            Forms.Dropdown.bind(this, "button", undefined)
                        ),
                    ],
                });
            } else {
                this.editor.form({
                    controls: [
                        new Forms.Static(
                            pgettext(
                                "block:radiobuttons",
                                "We need some buttons to let this work."
                            )
                        ),
                        new Forms.Spacer("small"),
                        new Forms.Button(
                            pgettext(
                                "block:radiobuttons",
                                "Manage radio buttons"
                            ),
                            "normal"
                        ).on(() => {
                            if (
                                this.node &&
                                this.node.layout instanceof Layouts.Node
                            ) {
                                this.node.layout.edit();
                            }
                        }),
                    ],
                });
            }
        }
    }
}
