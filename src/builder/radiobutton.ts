import {
    Collection,
    Forms,
    definition,
    editor,
    isFilledString,
    name,
    pgettext,
} from "tripetto";
import { Radiobuttons } from "./";

export class Radiobutton extends Collection.Item<Radiobuttons> {
    @definition
    @name
    name = "";

    @definition
    value = "";

    @editor
    defineEditor(): void {
        this.editor.option({
            name: "Label",
            form: {
                title: pgettext("block:radiobuttons", "Radio button label"),
                controls: [
                    new Forms.Text(
                        "singleline",
                        Forms.Text.bind(this, "name", "")
                    )
                        .autoFocus()
                        .autoSelect(),
                ],
            },
            locked: true,
        });

        this.editor.option({
            name: pgettext("block:radiobuttons", "Identifier"),
            form: {
                title: "Radio button identifier",
                controls: [
                    new Forms.Text(
                        "singleline",
                        Forms.Text.bind(this, "value", "")
                    ),

                    new Forms.Static(
                        pgettext(
                            "block:radiobuttons",
                            // tslint:disable-next-line:max-line-length
                            "If a radio button identifier is set, this identifier will be used as selected value instead of the button label."
                        )
                    ),
                ],
            },
            activated: isFilledString(this.value),
        });
    }
}
