/** Package information retrieved from `package.json` using webpack. */
declare const PACKAGE_NAME: string;

/** Dependencies */
import {
    ConditionBlock,
    condition,
    tripetto,
} from "tripetto-runner-foundation";

@tripetto({
    type: "condition",
    identifier: PACKAGE_NAME,
})
export class RadiobuttonCondition extends ConditionBlock<{
    readonly button: string;
}> {
    @condition
    isSelected(): boolean {
        const radioSlot = this.valueOf<string>();

        return (
            (radioSlot && radioSlot.reference === this.props.button) || false
        );
    }
}
