export interface IRadiobutton {
    /** Id of the button. */
    readonly id: string;

    /** Name of the button. */
    readonly name: string;

    /** Value of the button. */
    readonly value: string;
}
