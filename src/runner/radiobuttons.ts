/** Dependencies */
import {
    NodeBlock,
    Slots,
    arrayItem,
    assert,
    findFirst,
} from "tripetto-runner-foundation";
import { IRadiobutton } from "./radiobutton";

export abstract class Radiobuttons extends NodeBlock<{
    buttons: IRadiobutton[];
}> {
    /** Contains the radio buttons slot. */
    readonly radioSlot = assert(
        this.valueOf<string, Slots.String>("button")
    ).confirm();

    /** Contains if the radio button is required. */
    readonly required = this.radioSlot.slot.required || false;

    /** Retrieves the reference of the selected button. */
    get value() {
        const selected = findFirst(
            this.props.buttons,
            (button: IRadiobutton) => button.id === this.radioSlot.reference
        );

        if (!selected && this.required && this.props.buttons.length > 0) {
            return this.select(this.props.buttons[0]);
        }

        return (selected && selected.id) || "";
    }

    /** Sets the reference of the selected button. */
    set value(reference: string) {
        const selected = findFirst(
            this.props.buttons,
            (button: IRadiobutton) => button.id === reference
        );

        if (selected) {
            this.select(selected);
        }
    }

    /** Verifies if a button is selected. */
    isSelected(button: IRadiobutton): boolean {
        return this.value === button.id;
    }

    /** Selects a button. */
    select(button: IRadiobutton): string {
        this.radioSlot.set(button.value || button.name, button.id);

        return button.id;
    }
}
